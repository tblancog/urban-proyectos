<?php

use Faker\Generator as Faker;
use App\Property;

$factory->define(Property::class, function (Faker $faker) {
  $propertyName = $faker->unique()->numerify('Property ####');
    return [
        'name' => $propertyName,
        'description' => $faker->paragraph(3),
        'status'=> 'obra',
        'address' => $faker->streetAddress(),
        'contact_email' => $faker->freeEmail(),
        'price' => $faker->randomNumber(6),
        'image_name' => $faker->imageUrl(800, 600, 'city'),
        'user_id'=> 1
    ];
});
