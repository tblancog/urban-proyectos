@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!--Carousel Wrapper-->
            <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                <!--Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                    <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                </ol>
                <!--/.Indicators-->
                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <!--First slide-->
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(130).jpg"
                            alt="First slide">
                    </div>
                    <!--/First slide-->
                    <!--Second slide-->
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(129).jpg"
                            alt="Second slide">
                    </div>
                    <!--/Second slide-->
                    <!--Third slide-->
                    <div class="carousel-item">
                        <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg"
                            alt="Third slide">
                    </div>
                    <!--/Third slide-->
                </div>
                <!--/.Slides-->
                <!--Controls-->
                <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->

            <div class="card">
                {{-- <img class="card-img-top" src="https://picsum.photos/id/743/1000/350" alt="Card image cap"> --}}
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h1>{{ $property->name }}</h1>
                        <div class="ml-auto">
                            <a href="{{ route('properties.index') }}" class="btn btn-outline-secondary">Volver </a>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    {!! $property->description !!}
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Dirección: {{ $property->address }}</li>
                    <li class="list-group-item">Precio: {{ $property->price }}</li>
                    <li class="list-group-item">Contacto: {{ $property->contact_email }}</li>
                    <li class="list-group-item">Imagen: {{ $property->image_name }}</li>
                    <li class="list-group-item">Estado: {{ $property->status }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
