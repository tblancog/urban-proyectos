@extends('layouts.dashboard')

@section('content')

<div class="container prop-container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h2>Propiedades</h2>
                        <div class="ml-auto">
                            <a href="{{ route('properties.create') }}" class="btn btn-verde">Crear propiedad</a>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    @include ('layouts._messages')
                    @foreach ($items as $item)
                        <div class="media">
                            <div class="d-flex flex-column counters">
                                <img class="card-img-top" src="https://picsum.photos/id/743/200/120" alt="Card image cap">
                            </div>
                            <div class="media-body">
                                <div class="d-flex align-items-center">
                                    <h3 class="mt-0"><a href="{{ $item->url }}">{{ $item->name }}</a></h3>
                                    <div class="ml-auto">
                                        {{-- @can ('update', $item) --}}
                                            <a href="{{ route('properties.edit', $item->id) }}" class="btn btn-verde">Editar</a>
                                        {{-- @endcan
                                        @can ('delete', $item) --}}
                                            <form class="form-delete" method="post" action="{{ route('properties.destroy', $item->id) }}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-gris" onclick="return confirm('Estás seguro?')">Eliminar</button>
                                            </form>
                                        {{-- @endcan --}}
                                    </div>
                                </div>
                                {{-- <p class="lead">
                                    Asked by 
                                    <a href="{{ $item->user->url }}">{{ $item->user->name }}</a> 
                                    <small class="text-muted">{{ $item->created_date }}</small>
                                </p> --}}
                                {{ str_limit($item->description, 250) }}
                            </div>                        
                        </div>
                        <hr>
                    @endforeach

                    <div class="mx-auto">
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
