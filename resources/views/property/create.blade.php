@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header create-header">
                    <div class="d-flex align-items-center">
                        <h2>Crear propiedad</h2>
                        <div class="ml-auto">
                            <a href="{{ route('properties.index') }}" class="btn btn-outline-secondary">Mostrar todas las propiedades</a>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                   <form action="{{ route('properties.store') }}" method="post">
                        @include ("property._form", ['buttonText' => "Crear propiedad"])
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
