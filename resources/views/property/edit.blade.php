@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div class="container property-cont">
                <img class="card-img-top" src="https://picsum.photos/id/743/1000/350" alt="Card image cap">
                <div class="ml-auto">
                      <!-- Button trigger modal -->
                      <div class="ml-auto">
                        <a href="#" class="btn btn-outline-secondary" data-toggle="modal" data-target="#exampleModalLong">Cargar imágenes</a>
                      </div>
                    </div>
                  </div>
              </div>
                <div class="card-header">
                  <div class="d-flex align-items-center">
                    <h2> Editar Propiedad </h2>
                    <div class="ml-auto">
                      <a href="{{ route('properties.index') }}" class="btn btn-outline-secondary">Volver</a>
                    </div>
                  </div>
                </div>

                <div class="card-body">
                <form action="{{ route('properties.update', $property->id) }}" method="post">
                  {{ method_field('PUT') }}
                  @include('property._form', ['buttonText'=> 'Guardar'])
                </form>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Carga hasta 5 imágenes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <image-uploader-component></image-uploader-component>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection