<div class="container home-side">
    <div class="row">
        <div class="col-md-3 section-l">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Propiedades</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Nosotros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Inversiones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Crédito</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tasaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
              </ul>
        </div>
        <div class="col-md-9 section-r">
            
        </div>
    </div>
</div>

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/side-styles.css') }}">
@endpush
