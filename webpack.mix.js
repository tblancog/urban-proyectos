const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.
// 'resources/assets/js/app.js', 'public/js'
   js('resources/js/app.js', 'public/js')
  //  .copy('resources/js/', 'public/js/')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/dashboard.scss', 'public/css')
   .sass('resources/sass/side-styles.scss', 'public/css')
   .copy('resources/fonts/**/*', 'public/fonts')
   .copy('resources/img/', 'public/img') // site images
   .copy('resources/images/', 'public/images') // vendor images


