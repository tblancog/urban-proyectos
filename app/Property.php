<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
      'name',
      'description' ,
      'address',
      'price' ,
      'contact_email',
      'image_name',
      'status'
    ];
    /**
     * Slug attribute
     */
    public function setNameAttribute($name){

      $this->attributes['name'] = $name;
      $this->attributes['slug'] = str_slug($name);
    }
    
    /**
     * Gets date of creation attribute
     * 
     */
    public function getDateCreatedAttribute(){
      
      return $this->created_at->diffForHumans();
    }

    /**
     * Get user for this property
     * 
     */
    public function user(){

      return $this->belongsTo(User::class);
    }

    /**
     * Gets Url attribute
     *
     * @return url
     */
    public function getUrlAttribute()
    {
        return route("properties.show", $this->slug);
    }
}
