<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('properties', 'PropertyController')->except(['show'])->middleware('auth');
Route::get('properties/{slug}', 'PropertyController@show')->name('properties.show')->middleware('auth');

// Static pages
Route::get('/detail', function () {
    return view('detail');
});

Route::get('/developers', function () {
    return view('index-developers');
});

Route::get('/houses', function () {
    return view('index-houses');
});

Route::get('/developers_details', function () {
    return view('developers-details');
});

Route::get('/houses_details', function () {
    return view('houses-details');
});
